class Animal:
    def __init__(self, name, breed, age):
        self._name = name
        self._breed = breed
        self._age = age

    def eat(self, food):
        pass

    def make_sound(self):
        pass

    def call(self):
        pass
    
    # Getter
    def get_name(self, name):
        return self._name

    def get_breed(self, breed):
        return self._breed

    def get_age(self, age):
        return self._age

    # Setter
    def set_name(self, name):
        self._name = name

    def set_breed(self, breed):
        self._breed = breed

    def set_age(self, age):
        self._age = age

   

class Cat(Animal):
    def eat(self, food):
        print(f"Eaten {food}")

    def make_sound(self):
        print("Miaow! Nyaw! Nyaaaaa!")

    def call(self):
        print(f"{self._name}, Come on!")

class Dog(Animal):
    def eat(self, food):
        print(f"Eaten {food}")

    def make_sound(self):
        print("Bark! Woof! Arf!")

    def call(self):
        print(f"Here {self._name}!")

# Test Cases:
dog1 = Dog("Isis", "Dalmatian", 15)
dog1.eat("Steak")      
dog1.make_sound()      
dog1.call()            

cat1 = Cat("Puss", "Persian", 4)
cat1.eat("Tuna")       
cat1.make_sound()      
cat1.call()            
